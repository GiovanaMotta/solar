import numpy as np
from pathlib import Path
import pickle

from constants import sv, sr, sm
from initial import RandomGalaxy
from plotting import get_bounds, plot
from visual.images import create_image


class SystemConfig:

    def __init__(self,
                 dt=3600*24*365*1000*1000,
                 epsilon=1e20,
                 theta=0.8):
        self.dt = dt
        self.epsilon = epsilon
        self.theta = theta

    def __eq__(self, other):
        if not isinstance(other, SystemConfig):
            raise NotImplementedError

        return self.dt == other.dt and \
            self.epsilon == other.epsilon and \
            self.theta == other.theta

    def __str__(self):
        return f'SystemConfig_{self.dt}_{self.epsilon}_{self.theta}'


class System:

    def __init__(self,
                 force_function,
                 galaxy=RandomGalaxy(n=1024),
                 config=SystemConfig(),
                 pickle_file=None,
                 pickle_dir=None):
        self.r = galaxy.r
        self.galaxy = galaxy
        self.config = config
        self.F_params = np.array([config.epsilon, config.theta], dtype=np.float64)
        self.step = 0
        self.pickle_step = 0
        self._F = force_function
        self.calculate_bounds()
        self.pickle_file = pickle_file
        self.pickle_dir = pickle_dir
        self.load_pickle()

    def load_pickle(self):
        if self.pickle_file is None:
            if self.pickle_dir is None:
                self.r_steps = None
                self.pickle_path = None
                return
            else:
                self.pickle_file = Path(self.pickle_dir) / self.generate_pickle_path()

        self.pickle_path = Path(self.pickle_file)

        if self.pickle_path.exists():
            with self.pickle_path.open('rb') as f:
                config, self.r_steps = pickle.load(f)
            if config != self.config:
                raise ValueError('Loaded system configuration is not the same!')
            if not np.array_equal(self.r_steps[0], self.r):
                raise ValueError('Initial conditions that are loaded are not the same!')

            self.pickle_step = self.r_steps.shape[0]
        else:
            self.r_steps = np.array(self.r.copy())

    def generate_pickle_path(self):
        return f'{self.galaxy}/{self.config}.pickle'

    def save_pickle(self):
        if len(self.r_steps.shape) < 3 or \
                self.step + 1 > self.r_steps.shape[0]:
            raise ValueError('Not all steps are saved!')

        if self.pickle_step >= self.r_steps.shape[0]:
            # Image on disk is larger or equal to the one
            # we are trying to save
            return

        self.pickle_path.parent.mkdir(parents=True, exist_ok=True)
        with self.pickle_path.open('wb') as f:
            pickle.dump([self.config, self.r_steps], f)

    def F(self):
        return self._F(self.r, self.F_params)

    def do_steps(self, steps, save_vector=False):
        if self.r_steps is not None and \
            len(self.r_steps.shape) == 3 and \
                self.step + steps < self.r_steps.shape[0]:
            self.r = self.r_steps[self.step + steps, :, :]
        else:
            # Do initial leapfrog step
            if self.step == 0:
                self.do_leapfrog_step()

            for t in range(steps):
                self.r[:, sr] += self.config.dt * self.r[:, sv]
                self.r[:, sv] += self.config.dt * self.F()

                if save_vector:
                    if not self.pickle_path:
                        raise ValueError('Trying to save vector but no pickle_file is defined')

                    self.r_steps = np.append(self.r_steps, self.r.copy()).reshape([-1] + list(self.r.shape))

        self.step += steps

    def undo_leapfrog_step(self):
        self.r[:, sv] -= 0.5 * self.config.dt * self.F()

    def do_leapfrog_step(self):
        self.r[:, sv] += 0.5 * self.config.dt * self.F()

    def calculate_bounds(self):
        self.bounds = get_bounds(self.r)

    def plot(self):
        plot(self, self.bounds)

    def create_image(self, **kwargs):
        return create_image(self.r, self.bounds, **kwargs)

    def create_image_sequence(self, steps, include_first=False, step_kwargs={}, image_kwargs={}):
        if include_first:
            yield self.create_image(**image_kwargs)
        for _ in range(steps):
            self.do_steps(1, **step_kwargs)
            yield self.create_image(**image_kwargs)

    def angular_momentum(self):
        return np.sum(
            self.r[:, sm] * np.cross(self.r[:, sr], self.r[:, sv])
        )

    def angular_momentum_sequence(self, steps, include_first=True, **kwargs):
        if include_first:
            yield self.angular_momentum()
        for _ in range(steps - include_first):
            self.do_steps(1, **kwargs)
            yield self.angular_momentum()

    def momentum(self):
        return np.sum(
            self.r[:, sm] * np.linalg.norm(self.r[:, sv], axis=1)
        )

    def momentum_sequence(self, steps, include_first=True, **kwargs):
        if include_first:
            yield self.momentum()
        for _ in range(steps - include_first):
            self.do_steps(1, **kwargs)
            yield self.momentum()

    def kinetic_energy(self):
        return 0.5 * np.sum(
            np.linalg.norm(self.r[:, sv], axis=1)**2 * self.r[:, sm]
        )

    def kinetic_energy_sequence(self, steps, include_first=True, **kwargs):
        if include_first:
            yield self.kinetic_energy()
        for _ in range(steps - include_first):
            self.do_steps(1, **kwargs)
            yield self.kinetic_energy()
