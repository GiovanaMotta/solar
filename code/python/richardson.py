import numpy as np
from copy import deepcopy

from initial import MilkyWay
from system import SystemConfig, System
from constants import dim


def test_order(force_function, steps=1, galaxy=MilkyWay(64), config=SystemConfig()):
    n = galaxy.r.shape[0]
    errors = np.zeros((3, n, dim*2))

    for i in range(3):
        current_config = deepcopy(config)
        current_config.dt = config.dt * 2**(-i)

        system = System(galaxy=deepcopy(galaxy),
                        force_function=force_function,
                        config=current_config)

        system.do_steps(steps * 2**i)
        system.undo_leapfrog_step()
        errors[i] = system.r[:, :dim*2]

    Qh = errors[0]
    Q12h = errors[1]
    Q14h = errors[2]
    orders = np.log((Qh - Q12h) / (Q12h - Q14h)) / np.log(2.)
    return orders
