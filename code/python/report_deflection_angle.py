from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt

from system import SystemConfig, System
from initial import RandomGalaxiesColission, RandomGalaxiesCollisionInverse
from force import treecode_simple

from constants import solar_mass, svx, svy, G, milky_way_radius

n1 = 1024
n2 = 1024
steps = 1000
dt = 3600 * 24 * 365 * 1000 * 1000 * 100
theta = 0.8
image_shape = (1080, 1080)

pickle_dir = Path.cwd() / 'data'
speeds = [500, 1000, 1500, 2000, 3000, 4000]
galaxies = [RandomGalaxiesColission, RandomGalaxiesCollisionInverse]
galaxy_names = ['Same rotation', 'Opposite rotation']
galaxy_lines = ['-', '--']
speeds = speeds[::-1]

close_steps = 600

angles = np.zeros((len(speeds), len(galaxies), 2), dtype=np.float64)

for galaxy_index, (galaxy, galaxy_name) in enumerate(zip(galaxies, galaxy_names)):
    for i, speed in enumerate(speeds):
        closeness = np.zeros((steps, n1 + n2, 2), dtype=np.uint16)
        system = System(force_function=treecode_simple.F,
                        galaxy=galaxy(
                            n1, n2,
                            mass=100*solar_mass,
                            black_hole_mass=4e6*solar_mass,
                            speed=speed
                        ),
                        config=SystemConfig(
                            theta=theta,
                            dt=dt,
                            epsilon=1e32
                        ),
                        pickle_dir=pickle_dir)

        min_dist = 1e100
        min_dist_index = 0

        for index in range(steps):
            system.do_steps(1, save_vector=True)
            r = system.r

            if index % 50 == 0:
                print(f'[{index+1}/{steps}] with speed {speed} galaxy {galaxy}')
                system.save_pickle()

        system.save_pickle()
        print(f'[{steps}/{steps}] with speed {speed} galaxy {galaxy}')

        angle = np.arctan2(r[(0, n1), svy], r[(0, n1), svx])
        angles[i, galaxy_index, :] = angle

fig = plt.figure(figsize=(8, 5))
ax = fig.add_subplot(111)

colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][:len(speeds)]*2

s1 = ax.scatter(speeds,
                (angles[:, 0, 0]+angles[:, 0, 1]+np.pi)/2, color=colors[0], marker='+')
s2 = ax.scatter(speeds,
                (angles[:, 1, 0]+angles[:, 1, 1]+np.pi)/2, color=colors[1], marker='x')

x = np.linspace(speeds[0], speeds[-1])
real = ax.plot(x, 2*np.arctan(G*8e6*solar_mass/(milky_way_radius * x**2)), ls='--', c='grey')

ax.legend([s1, s2, real[0]], ['Same rotation', 'Opposite rotation', 'Analytical'])

ax.set_ylabel('Deflection angle (radians)')
ax.set_xlabel('Speed (m/s)')

fig.tight_layout()

figure_folder = Path.cwd() / 'report' / 'figures'
figure_folder.mkdir(exist_ok=True, parents=True)

fig.savefig(figure_folder / 'plot_deflection_angle.png',
            dpi=200,
            transparent=True)

plt.show()
