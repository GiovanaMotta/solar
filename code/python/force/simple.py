import numpy as np
import numba

from constants import dim, sm, G


@numba.jit(
    numba.float64[:, :](numba.float64[:, :], numba.float64[:]),
    nopython=True, parallel=True
)
def F(r, params):
    """Calculates the force on each particle

    params:
        0: softening factor (epsilon)
    """
    n = r.shape[0]
    f = np.zeros((n, dim), dtype=np.float64)

    for i in numba.prange(n):
        for j in range(n):
            if j == i:
                continue

            diff = r[j, 0:dim] - r[i, 0:dim]
            diff_2_norm = diff[0]*diff[0] + diff[1]*diff[1]

            f[i] += r[j, sm] * diff / (diff_2_norm + params[0])**(3/2)

        f[i] *= G

    return f
