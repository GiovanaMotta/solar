from matplotlib import pyplot as plt
import numpy as np

from system import System, SystemConfig
from initial import MilkyWay
from force import treecode_simple, simple
from richardson import test_order

config = SystemConfig(
    dt=3600*24*365*1000*100,
    epsilon=1e20,
    theta=0.8
)

simulation = System(galaxy=MilkyWay(n=128),
                    config=config,
                    force_function=treecode_simple.F)

# Plot using matplotlib
simulation.plot()

# Do n steps
simulation.do_steps(1)

# Create image using opencv
for image in simulation.create_image_sequence(steps=2, include_first=True):
    # Show opencv image using matplotlib
    plt.imshow(image, cmap='gray', vmin=0, vmax=255)
    plt.show()


# Richardson order test
config = SystemConfig(
    dt=3600*24*365*1000*100,
    epsilon=1e30,
    theta=0.6
)

galaxy = MilkyWay(n=64)

orders = test_order(treecode_simple.F, steps=1, galaxy=galaxy, config=config)
print(np.nanmean(orders, axis=0))
print(np.nanmin(orders, axis=0))
print(np.nanmax(orders, axis=0))

orders = test_order(simple.F, steps=1, galaxy=galaxy, config=config)
print(np.nanmean(orders, axis=0))
print(np.nanmin(orders, axis=0))
print(np.nanmax(orders, axis=0))
