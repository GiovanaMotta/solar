import numba
import numpy as np

from constants import dim, sx, sy, sm, G


@numba.jit(
    numba.boolean(
        numba.float64[:], numba.float64[:, :],
        numba.float64, numba.float64, numba.float64, numba.float64, numba.float64[:]
    ),
    nopython=True
)
def check(ri, others, min_x, min_y, max_x, max_y, params):
    size_squared = (max_x - min_x)**2 + (max_y - min_y)**2

    mid_x, mid_y = (max_x + min_x) / 2, (max_y + min_y) / 2
    distance_squared = (ri[0] - mid_x)**2 + (ri[1] - mid_y)**2

    return size_squared / distance_squared < params[1]**2


@numba.jit(
    numba.float64[:](
        numba.float64[:], numba.float64[:, :], numba.float64[:]
    ),
    nopython=True
)
def tree(ri, others, params):
    if others.shape[0] == 0:
        return np.zeros(dim, dtype=np.float64)

    if others.shape[0] == 1:
        diff = others[0, 0:dim] - ri[0:dim]
        diff_2_norm = diff[0]*diff[0] + diff[1]*diff[1]

        return others[0, sm] * diff / (diff_2_norm + params[0])**(3/2)

    min_x, min_y = np.min(others[:, sx]), np.min(others[:, sy])
    max_x, max_y = np.max(others[:, sx]), np.max(others[:, sy])

    if check(ri, others, min_x, min_y, max_x, max_y, params):
        mass = np.sum(others[:, sm])

        center_of_mass = np.array([
            np.sum(others[:, sx] * others[:, sm]),
            np.sum(others[:, sy] * others[:, sm]),
        ]) / mass

        diff = center_of_mass - ri[0:dim]
        diff_2_norm = diff[0]*diff[0] + diff[1]*diff[1]

        return mass * diff / (diff_2_norm + params[0])**(3/2)
    else:
        mid_x, mid_y = (max_x + min_x) / 2, (max_y + min_y) / 2

        left = others[:, sx] < mid_x
        right = ~left
        bottom = others[:, sy] < mid_y
        top = ~bottom

        return (
            tree(ri, others[left & bottom], params) +
            tree(ri, others[left & top], params) +
            tree(ri, others[right & bottom], params) +
            tree(ri, others[right & top], params)
        )


@numba.jit(
    numba.float64[:, :](numba.float64[:, :], numba.float64[:]),
    nopython=True, parallel=True
)
def F(r, params):
    """Calculates the force on each particle using simple treecode

    params:
        0: softening factor (epsilon)
        1: maximum angle (theta)
    """
    n = r.shape[0]
    f = np.zeros((n, dim), dtype=np.float64)

    for i in numba.prange(n):
        other_i = np.ones(n, dtype=np.dtype('?'))
        other_i[i] = False

        others = r[other_i]
        ri = r[i]

        left = others[:, sx] < ri[sx]
        right = ~left
        bottom = others[:, sy] < ri[sy]
        top = ~bottom

        f[i] = G * (
            tree(ri, others[left & bottom], params) +
            tree(ri, others[left & top], params) +
            tree(ri, others[right & bottom], params) +
            tree(ri, others[right & top], params)
        )

    return f
