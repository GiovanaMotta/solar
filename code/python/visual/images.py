import cv2
import numpy as np

from constants import sx, sy


def create_image(r, bounds, shape=(1080, 1920)):
    img = np.zeros(shape[:2], np.uint8)

    min_y, max_y, min_x, max_x = bounds
    width_x = max_x - min_x
    width_y = max_y - min_y

    ratio_img = shape[0]/shape[1]
    ratio_bounds = width_x / width_y

    if ratio_img >= ratio_bounds:
        mid_x = min_x + width_x/2
        width_x *= ratio_img / ratio_bounds
        min_x, max_x = mid_x - width_x/2, mid_x + width_x/2
    elif ratio_img < ratio_bounds:
        mid_y = min_y + width_y/2
        width_y *= ratio_bounds / ratio_img
        min_y, max_y = mid_y - width_y/2, mid_y + width_y/2

    for rx, ry in r[:, (sy, sx)]:
        x = np.floor((rx - min_x) * shape[0] / width_x).astype(int)
        y = np.floor((max_y - ry) * shape[1] / width_y).astype(int)

        # Out of image bounds
        if x < 0 or x >= shape[0] or y < 0 or y >= shape[1]:
            continue

        if img[x, y] < 255:
            img[x, y] += 1

    r = 30
    kernel = cv2.circle(np.zeros((r, r), dtype=np.uint8),
                                (r//2, r//2), r//2, 1, -1)
    big_blur = cv2.filter2D(img, -1, kernel)

    r = 7
    kernel = cv2.circle(np.zeros((r, r), dtype=np.uint8),
                                (r//2, r//2), r//2, 1, -1)
    small_blur = cv2.filter2D(img, -1, kernel)

    _, small_blur = cv2.threshold(small_blur, 3, 255, cv2.THRESH_TRUNC)

    img = cv2.bitwise_and(small_blur, small_blur, mask=big_blur)

    alpha = 255 / np.max(img)
    img = (alpha * img).astype(np.uint8)

    r = 2
    img = cv2.GaussianBlur(img, (r*2+1, r*2+1), sigmaX=r/3, sigmaY=r/3)

    return img
