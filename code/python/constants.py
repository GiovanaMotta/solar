dim = 2
G = 6.67430e-11

milky_way_radius = 5e20  # in m
solar_mass = 2e30  # in kg

# Slices for mass
sm = 2*dim

# Slices for position
sr = slice(0, dim)
sx = 0
sy = 1

# Slices for velocity
sv = slice(dim, 2*dim)
svx = dim
svy = dim + 1
