from matplotlib import pyplot as plt
import numpy as np
from pathlib import Path

from system import SystemConfig, System
from initial import RandomGalaxy
from force import treecode_simple

from constants import solar_mass

n = 128
steps = 3000
dt = 3600 * 24 * 365 * 1000 * 1000
thetas = np.array([0.0, 0.7])
momenta = np.zeros((thetas.shape[0], steps), dtype=np.float64)
pickle_dir = Path.cwd() / 'data'

for index, theta in enumerate(thetas):
    print(f'[{index}/{thetas.shape[0]}] Starting with theta = {theta} ...')
    system = System(force_function=treecode_simple.F,
                    galaxy=RandomGalaxy(n, mass=solar_mass, black_hole_mass=4e6*solar_mass),
                    config=SystemConfig(
                        theta=theta,
                        dt=dt,
                        epsilon=1e20
                    ),
                    pickle_dir=pickle_dir)

    momenta[index] = [m for m in system.momentum_sequence(steps, save_vector=True)]
    system.save_pickle()

lines = plt.plot(momenta.T)

labels = [f'theta = {theta}' if theta > 0 else 'No tree code' for theta in thetas]
plt.legend(lines, labels)
plt.tight_layout()
plt.show()
