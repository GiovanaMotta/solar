import numpy as np

from matplotlib import pyplot as plt

from constants import sx, sy


def get_bounds(system):
    """Setup a dynamic plot using matplotlib"""
    axis = (sx, sy)

    if hasattr(system, 'r'):
        system = system.r

    min_x, min_y = np.min(system[:, axis], axis=0)
    max_x, max_y = np.max(system[:, axis], axis=0)
    bounds = [min_x, max_x, min_y, max_y]

    return bounds


def plot(system, bounds):
    """Plot the bodies"""
    if hasattr(system, 'r'):
        system = system.r

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(system[:, sx], system[:, sy], s=1, c='white', alpha=0.5)
    ax.set_facecolor('black')
    ax.axis(bounds)
    ax.set_aspect('equal')

    plt.tight_layout()
    plt.show()
