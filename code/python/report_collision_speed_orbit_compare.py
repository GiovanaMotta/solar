from pathlib import Path
from matplotlib.lines import Line2D
import numpy as np
from matplotlib import pyplot as plt

from system import SystemConfig, System
from initial import RandomGalaxiesColission, RandomGalaxiesCollisionInverse
from force import treecode_simple

from constants import solar_mass, sr

n1 = 1024
n2 = 1024
steps = 1000
dt = 3600 * 24 * 365 * 1000 * 1000 * 100
theta = 0.8
image_shape = (1080, 1080)

pickle_dir = Path.cwd() / 'data'
speeds = [500, 1000, 1500, 2000, 3000, 4000]
galaxies = [RandomGalaxiesColission, RandomGalaxiesCollisionInverse]
galaxy_names = ['Same rotation', 'Opposite rotation']
galaxy_lines = ['-', '--']
speeds = speeds[::-1]

close_steps = 600

number_closes = np.zeros((steps, len(galaxies), len(speeds)), dtype=np.uint16)

for galaxy_index, (galaxy, galaxy_name) in enumerate(zip(galaxies, galaxy_names)):
    for i, speed in enumerate(speeds):
        closeness = np.zeros((steps, n1 + n2, 2), dtype=np.uint16)
        system = System(force_function=treecode_simple.F,
                        galaxy=galaxy(
                            n1, n2,
                            mass=100*solar_mass,
                            black_hole_mass=4e6*solar_mass,
                            speed=speed
                        ),
                        config=SystemConfig(
                            theta=theta,
                            dt=dt,
                            epsilon=1e32
                        ),
                        pickle_dir=pickle_dir)

        min_dist = 1e100
        min_dist_index = 0

        for index in range(steps):
            print(f'[{index}/{steps}] with speed {speed} galaxy {galaxy}')
            system.do_steps(1, save_vector=True)
            r = system.r

            close_to_1 = np.linalg.norm(r[:, sr] - r[0, sr], axis=1) < 1.2 * system.galaxy.radius
            close_to_2 = np.linalg.norm(r[:, sr] - r[n1, sr], axis=1) < 1.2 * system.galaxy.radius

            closeness[index] = closeness[index - 1] + 1 if index > 0 else close_steps
            closeness[index, ~close_to_1, 0] = 0
            closeness[index, ~close_to_2, 1] = 0

            dist = np.linalg.norm(r[0, sr] - r[n1, sr])
            if dist < min_dist:
                min_dist = dist
                min_dist_index = index

            if index % 50 == 0:
                system.save_pickle()

        system.save_pickle()

        number_close = np.sum(closeness >= close_steps, axis=(1, 2))
        number_closes[:, galaxy_index, i] = number_close

fig = plt.figure(figsize=(8, 5))
ax = fig.add_subplot(111)

colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][:len(speeds)]*2

lines = ax.plot(np.arange(steps)*dt / (3600*24*365*1000*1000*1000),
                number_closes.reshape(steps, -1))

for i, line in enumerate(lines):
    line.set_linestyle('-' if i < len(speeds) else '--')
    line.set_color(colors[i])

lines = lines[:len(speeds)]
labels = [f'v = {speed} m/s' for speed in speeds]

lines += [
    Line2D([0], [0], color='white'),
    Line2D([0], [0], color='grey', lw=1.5, linestyle='-'),
    Line2D([0], [0], color='grey', lw=1.5, linestyle='--'),
]
labels += ['']
labels += galaxy_names

ax.legend(lines, labels)

ax.set_xlabel('Time (Gy)')
ax.set_ylabel('# of stars in orbit')

yticks = [0, 500, 1000, 1500, 2000]
ax.set_yticks(yticks)
ax.set_yticklabels([str(ytick) for ytick in yticks])

fig.tight_layout()

figure_folder = Path.cwd() / 'report' / 'figures'
figure_folder.mkdir(exist_ok=True, parents=True)

fig.savefig(figure_folder / 'plot_collission_orbit_speed_both.png',
            dpi=200,
            transparent=True)

plt.show()
