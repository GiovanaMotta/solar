# Simulating Galaxy Collisions UsingTree-Code

This is the code used to simulate galactic collisions. The relevant code can be found in `./code/python/`.

This Python implementation is a full package to easily run a number of different simulations.