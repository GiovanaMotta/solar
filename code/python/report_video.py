from pathlib import Path
import cv2
import os

from system import SystemConfig, System
from initial import MilkyWay
from force import treecode_simple

n = 2048
steps = 100
dt = 3600 * 24 * 365 * 1000 * 1000 * 20
pickle_dir = Path.cwd() / 'data'
image_shape = (1080, 1920)

system = System(force_function=treecode_simple.F,
                galaxy=MilkyWay(n, include_halo=True),
                config=SystemConfig(
                    theta=0.8,
                    dt=dt,
                    epsilon=1e20
                ),
                pickle_dir=pickle_dir)

image_dir = system.pickle_path.parent / system.pickle_path.stem
image_dir.mkdir(exist_ok=True, parents=True)

for index in range(steps):
    system.do_steps(1, save_vector=True)
    image_path = image_dir / f'{index:03d}.png'
    if not image_path.exists():
        image = system.create_image(shape=image_shape)
        cv2.imwrite(str(image_path), image)
        print(f'[{index+1}/{steps}]')

    if index % 25 == 0:
        system.save_pickle()

system.save_pickle()

ffmpeg_path = Path.cwd() / 'bin' / 'ffmpeg.exe'
movie_dir = system.pickle_path.parent
movie_dir.mkdir(exist_ok=True)
movie_path = movie_dir / (system.pickle_path.stem + '.mp4')

movie_dir.mkdir(exist_ok=True)

command = f'cd {image_dir} && {ffmpeg_path} -y -framerate 24 -i %03d.png -pix_fmt yuv420p {movie_path}'

os.system(command)
