import numpy as np

from richardson import test_order
from force import treecode_simple
from initial import RandomGalaxy
from system import System, SystemConfig

from constants import solar_mass

dt = 3600 * 24 * 365 * 1000 * 1000
n = 128

config = SystemConfig(
            theta=0.0,
            dt=dt,
            epsilon=1e32
)

system = System(force_function=treecode_simple.F,
                galaxy=RandomGalaxy(n, mass=solar_mass, black_hole_mass=4e6*solar_mass),
                config=config)

system.do_steps(100)
system.undo_leapfrog_step()

galaxy = RandomGalaxy(n, mass=solar_mass, black_hole_mass=4e6*solar_mass)
galaxy.r = system.r

result = test_order(treecode_simple.F, galaxy=galaxy, config=config)
print(result)

print(np.mean(result, axis=0))
print(np.std(result, axis=0))

sort = np.sort(result, axis=0)
sort = sort[8:-8]

print(np.mean(sort))
print(np.std(sort))
