from pathlib import Path
import cv2
import os
import platform
import numpy as np

from initial import ImageGalaxy
from system import System, SystemConfig
from force import treecode_simple

image_path = Path.cwd() / 'code' / 'data' / 'luffie.png'
steps = 300
dt = 3600 * 24 * 365 * 1000 * 1000 * 50
pickle_dir = Path.cwd() / 'data'
image_shape = (1080, 1920)

galaxy = ImageGalaxy(image_path, x=192, y=108, threshold=150)
system = System(force_function=treecode_simple.F,
                galaxy=galaxy,
                config=SystemConfig(
                    theta=0.8,
                    dt=dt,
                    epsilon=1e32
                ),
                pickle_dir=pickle_dir)

# Set plotting bounds to the image bounds
system.bounds = galaxy.bounds

print(f'Number of stars: {galaxy.n}')

image_dir = system.pickle_path.parent / system.pickle_path.stem
image_dir.mkdir(exist_ok=True, parents=True)

for index in range(steps):
    image_path = image_dir / f'{index:03d}.png'
    if not image_path.exists():
        image = system.create_image(shape=image_shape)
        cv2.imwrite(str(image_path), image)
        print(f'[{index+1}/{steps}]')

    system.do_steps(1, save_vector=True)
    if index % 10 == 0:
        system.save_pickle()

system.save_pickle()

ffmpeg_path = 'ffmpeg'
if platform.system() == 'Windows':
    ffmpeg_path = Path.cwd() / 'bin' / 'ffmpeg.exe'

movie_dir = system.pickle_path.parent
movie_dir.mkdir(exist_ok=True)
movie_path = movie_dir / (system.pickle_path.stem + '.mp4')

movie_dir.mkdir(exist_ok=True)

command = f'cd {image_dir} && {ffmpeg_path} -y -framerate 60 -i %03d.png -pix_fmt yuv420p -vf reverse {movie_path}'

os.system(command)
