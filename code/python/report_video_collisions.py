from pathlib import Path
import cv2
import os

from system import SystemConfig, System
from initial import RandomGalaxiesColission
from force import treecode_simple

from constants import solar_mass

n1 = 1024
n2 = 1024
steps = 800
dt = 3600 * 24 * 365 * 1000 * 1000 * 100
theta = 0.8
image_shape = (1080, 1080)

pickle_dir = Path.cwd() / 'data'

system = System(force_function=treecode_simple.F,
                galaxy=RandomGalaxiesColission(
                    n1, n2,
                    mass=100*solar_mass,
                    black_hole_mass=4e6*solar_mass,
                    speed=2000
                ),
                config=SystemConfig(
                    theta=theta,
                    dt=dt,
                    epsilon=1e20
                ),
                pickle_dir=pickle_dir)

image_dir = system.pickle_path.parent / system.pickle_path.stem
image_dir.mkdir(exist_ok=True, parents=True)

for index in range(steps):
    system.do_steps(1, save_vector=True)
    image_path = image_dir / f'{index:03d}.png'
    if not image_path.exists():
        image = system.create_image(shape=image_shape)
        cv2.imwrite(str(image_path), image)
        print(f'[{index+1}/{steps}]')

    if index % 50 == 0:
        system.save_pickle()

system.save_pickle()

ffmpeg_path = Path.cwd() / 'bin' / 'ffmpeg.exe'
movie_dir = system.pickle_path.parent
movie_dir.mkdir(exist_ok=True)
movie_path = movie_dir / (system.pickle_path.stem + '.mp4')

movie_dir.mkdir(exist_ok=True)

command = f'cd {image_dir} && {ffmpeg_path} -y -framerate 20 -i %03d.png -pix_fmt yuv420p -crf 1 {movie_path}'

os.system(command)
