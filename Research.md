Hubble Classification for galaxies:
https://en.wikipedia.org/wiki/Galaxy#/media/File:Hubble_sequence_photo.png

3 types of galaxies: Elliptical, spiral, and irregular

Elliptical galaxies:
    
*  Contain very little interstellar matter, resulting in low star formation
    *  This leads to older stars on average
*  Motion is predominantly radial
*  Every massive elliptical galaxy has a supermassive black hole in it.
    *  The mass of the black hole is correlated with the mass of the galaxy
*  elliptical galaxies are more three-dimensional, without much structure, and their stars are in somewhat random orbits around the center.
*  Classification determined by the ratio between the major and minor axes of the ellipse
*  Elliptical galaxies mainly form through the merger of two galaxies
*  Shell galaxies are a subclass of elliptical galaxies where stars in the galaxy's halo are grouped in concentric shells

Spiral galaxies:
*  Two primary forms: Spiral and barred spiral
*  Spiral galaxies primarily lie in a plane
    *  they are flat and rotating disks which contain a bulge
*  Classification depends on how tightly wrapped the arms of the spiral are
*  Two theories regarding the formation of spirals: Density wave model and SSPSF model
*  The stars inside of the spiral changes, stars are in the spiral for a non-permanent amount of time.
*  Using Newton we find that the galaxies would rotate much slower than they do in reality. This is usually corrected using dark matter.
    *  Our galaxies would probably be small enough that this won't play a significant role, but a correction for this could be interesting to explore.
*  Bars act as a stellar nursery, allowing new stars to form


Galaxies contain millions of stars, our simulation obviously cannot be run with millions of points.

Galaxies have their own magnetic fields. This field is strong enough to be important and drive mass inflow into the center of the galaxy. They also modify the formation of the spiral arms


Many galaxies tend te be relatively close to disk-shaped as the angular momentum of individual particles will cancel out due to collisions. This means most particles end up having an angular momentum in the direction of the total angular momentum. Unless we want to model what specifically happens before the galaxies becomes disk-shaped, we can use a 2D simulation instead. This is faster and still fairly close to accurate.




-https://en.wikipedia.org/wiki/M%E2%80%93sigma_relation, relation between the mass of the black hole and velocity dispersion of a galaxy's bulge


Defs:
*  Velocity disperion: The statisical dispersion of velocities about the mean velocity of the group
*  Bulge: A central concentration of stars primarily found in spiral galaxies
    *  Two types: Classical and disk-like bulges
        *  Classical bulges have the spiral end at the edge of the bulges
        *  Disk-like bulges have the spiral continue in the bulge
*  Merger: When two galaxies collide

