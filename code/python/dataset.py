import numpy as np
import re

from pathlib import Path

from constants import sr, sv, sm, solar_mass, dim


class DubinskiDataset:
    star_data = None
    file_length = 81920

    def __init__(self):
        if DubinskiDataset.star_data is None:
            file_path = Path(__file__).parent.parent / 'data' / 'dubinski.tab.gz'

            star_data = np.zeros((DubinskiDataset.file_length, 7))
            with open(file_path) as lines:
                for i, line in enumerate(lines):
                    star_data[i] = np.array([float(val) for val in re.split(":? ", line, 7)[:-1]])

            # Move mass from first index (0) to last index (-1)
            # and shift the rest one to the left
            star_data[:, -1], star_data[:, 0:-1] = star_data[:, 0], star_data[:, 1:].copy()

            # The position data is in 4 kpc, convert it to m
            # The distance between milky way and andromeda is about 30
            # and the source states that they are 120 kpc away, so 10kpc
            # Also the source states 1 = 4kpc
            star_data[:, sr] *= 4 * 3.086e+19

            # Speed is in 220 km/s convert it to m/s
            # Source states 1 = 220 km/s
            star_data[:, sv] *= 220 * 1000

            # Mass is in 5.37e10 solar mass, convert it to kg
            # Sources states 0.82 = 4.4e10 solar mass
            star_data[:, sm] *= 5.36585e10 * solar_mass

            # Convert from 3 to `dim` dimensions
            DubinskiDataset.star_data = np.zeros((DubinskiDataset.file_length, dim*2 + 1))
            DubinskiDataset.star_data[:, sr] = star_data[:, 0:dim]
            DubinskiDataset.star_data[:, sv] = star_data[:, 3:dim+3]
            DubinskiDataset.star_data[:, sm] = star_data[:, 6]

            DubinskiDataset.star_data = star_data

    def _sample(self, data, n):
        if n is None:
            return data.copy()
        if n > data.shape[0]:
            raise ValueError(f'Cannot sample {n} points from a dataset of size {data.shape[0]}')

        part = np.round(np.linspace(0, data.shape[0], num=n, endpoint=False)).astype(int)

        sample_data = data[part].copy()

        # Account for less mass
        sample_data[:, sm] *= n / data.shape[0]

        return sample_data

    def get_data(self, n=None):
        return self._sample(DubinskiDataset.star_data, n)

    def get_milky_way_data(self, n=None, include_halo=False):
        milky_way_slice = np.zeros(DubinskiDataset.file_length, dtype=np.dtype('?'))
        milky_way_slice[:16384] = 1
        milky_way_slice[16384*2:16384*2+8192] = 1
        if include_halo:
            milky_way_slice[16384*3:16384*4] = 1

        milky_way_data = DubinskiDataset.star_data[milky_way_slice]

        return self._sample(milky_way_data, n)
