from matplotlib import pyplot as plt
import numpy as np
from pathlib import Path

from system import SystemConfig, System
from initial import MilkyWay
from force import treecode_simple

n = 256
steps = 100
dt = 3600 * 24 * 365 * 1000 * 100 * 3
thetas = np.array([0, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
momenta = np.zeros((thetas.shape[0], steps), dtype=np.float64)
pickle_dir = Path.cwd() / 'data'

for index, theta in enumerate(thetas):
    print(f'[{index}/{thetas.shape[0]}] Starting with theta = {theta} ...')
    system = System(force_function=treecode_simple.F,
                    galaxy=MilkyWay(n),
                    config=SystemConfig(
                        theta=theta,
                        dt=dt
                    ),
                    pickle_dir=pickle_dir)

    momenta[index] = [m for m in system.angular_momentum_sequence(steps, save_vector=True)]
    system.save_pickle()

lines = plt.plot(momenta.T)

labels = [f'theta = {theta}' if theta > 0 else 'No tree code' for theta in thetas]
plt.legend(lines, labels)
plt.tight_layout()
plt.show()
