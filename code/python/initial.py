import numpy as np
import random
import cv2

from pathlib import Path

from dataset import DubinskiDataset
from constants import sx, sy, sm, sv, svx, svy, sr, milky_way_radius, solar_mass, dim, G


class RandomGalaxy:

    def __init__(self, n, radius=milky_way_radius, mass=solar_mass, black_hole_mass=None):
        seed = 66743015
        random.seed(seed)
        np.random.seed(seed)
        self.n = n
        self.radius = radius
        self.mass = mass
        self.black_hole_mass = black_hole_mass
        # The array containing [x, y, vx, vy, m] for each body
        r = np.zeros((n, (dim * 2) + 1), dtype=np.float64)

        # Random stars in a rayleigh distribution
        s = np.random.rayleigh(scale=0.6, size=n)
        th = np.random.rand(n) * 2*np.pi
        r[:, sx] = radius/2 * s * np.cos(th)
        r[:, sy] = radius/2 * s * np.sin(th)
        r[:, sm] = mass

        if black_hole_mass is not None:
            r[0, sr] = 0
            r[0, sv] = 0
            r[0, sm] = black_hole_mass

        # Calculate (not yet) stable orbit speed
        rnorm = np.linalg.norm(r[:, sr], axis=1) + 1
        sort = np.argsort(rnorm)

        v = np.sqrt(G * (np.cumsum(r[sort, sm]) - r[sort, sm]) / rnorm)
        # Speed is vector perpendicular to r, with length v
        r[:, sv] = np.array([0, 1, -1, 0]).reshape(2, 2).dot((r[:, sr] * np.repeat(v / rnorm, 2).reshape(-1, 2)).T).T

        self.r = r

    def __str__(self):
        return f'RandomGalaxy_{self.n}_{self.radius}_{self.mass}_{self.black_hole_mass}'


class RandomGalaxiesColission:

    def __init__(self, n1, n2,
                 radius=milky_way_radius,
                 mass=solar_mass,
                 black_hole_mass=None,
                 distance=4*milky_way_radius,
                 collision_distance=milky_way_radius,
                 speed=5e4):
        self.n1 = n1
        self.n2 = n2
        self.radius = radius
        self.mass = mass
        self.black_hole_mass = black_hole_mass
        self.distance = distance
        self.collision_distance = collision_distance
        self.speed = speed
        r1 = RandomGalaxy(n1, radius=radius, mass=mass, black_hole_mass=black_hole_mass).r
        r2 = RandomGalaxy(n2, radius=radius, mass=mass, black_hole_mass=black_hole_mass).r

        r1[:, sx] -= distance / 2
        r1[:, sy] -= collision_distance / 2
        r1[:, svx] += speed / 2

        r2[:, sx] += distance / 2
        r2[:, sy] += collision_distance / 2
        r2[:, svx] -= speed / 2

        self.r = np.append(r1, r2).reshape(n1 + n2, -1)

    def __str__(self):
        return f'RandomGalaxiesCollision_{self.n1}_{self.n2}_' \
            f'{self.radius}_{self.mass}_{self.black_hole_mass}_' \
            f'{self.distance}_{self.collision_distance}_{self.speed}'


class RandomGalaxiesCollisionInverse:

    def __init__(self, n1, n2,
                 radius=milky_way_radius,
                 mass=solar_mass,
                 black_hole_mass=None,
                 distance=4*milky_way_radius,
                 collision_distance=milky_way_radius,
                 speed=5e4):
        self.n1 = n1
        self.n2 = n2
        self.radius = radius
        self.mass = mass
        self.black_hole_mass = black_hole_mass
        self.distance = distance
        self.collision_distance = collision_distance
        self.speed = speed

        r1 = RandomGalaxy(n1, radius=radius, mass=mass, black_hole_mass=black_hole_mass).r
        r2 = RandomGalaxy(n2, radius=radius, mass=mass, black_hole_mass=black_hole_mass).r

        r1[:, sx] -= distance / 2
        r1[:, sy] -= collision_distance / 2
        r1[:, svx] += speed / 2

        r2[:, sx] += distance / 2
        r2[:, sy] += collision_distance / 2
        r2[:, sv] *= -1  # Invert rotation
        r2[:, svx] -= speed / 2

        self.r = np.append(r1, r2).reshape(n1 + n2, -1)

    def __str__(self):
        return f'RandomGalaxiesCollisionInverse_{self.n1}_{self.n2}_' \
            f'{self.radius}_{self.mass}_{self.black_hole_mass}_' \
            f'{self.distance}_{self.collision_distance}_{self.speed}'


class MilkyWay:

    def __init__(self, n, include_halo=False):
        self.n = n
        self.include_halo = include_halo
        self.r = DubinskiDataset().get_milky_way_data(n=n, include_halo=include_halo)

    def __str__(self):
        halo = 'halo' if self.include_halo else 'nohalo'
        return f'MilkyWay_{self.n}_{halo}'


class DubinskiGalaxies:

    def __init__(self, n):
        self.n = n
        self.r = DubinskiDataset().get_data(n=n)

    def __str__(self):
        return f'DubinskiGalaxies_{self.n}'


class ImageGalaxy:

    def __init__(self, image_path, x, y, threshold=150):
        self.image_name = Path(image_path).stem
        self.x = x
        self.y = y
        self.threshold = threshold

        img = cv2.imread(str(image_path), cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (x, y))
        img = np.array(img)

        self.img = img

        scale = max(x, y)

        w, h = np.array(img.shape) / scale * milky_way_radius
        self.bounds = [-w, w, -h, h]

        ry, rx = np.where(img < threshold)
        self.n = rx.shape[0]

        r = np.zeros((self.n, (dim * 2) + 1), dtype=np.float64)
        r[:, sx] = -2 * (rx - (x/2)) / scale * milky_way_radius
        r[:, sy] = 2 * (ry - (y/2)) / scale * milky_way_radius
        r[:, sm] = solar_mass

        self.n += 1
        r = np.append(r, [0]*(dim*2) + [4e6*solar_mass]).reshape(self.n, -1)

        rnorm = np.linalg.norm(r[:, sr], axis=1) + 1
        sort = np.argsort(rnorm)

        v = np.sqrt(G * (np.cumsum(r[sort, sm]) - r[sort, sm]) / rnorm)
        r[:, sv] = np.array([0, -1, 1, 0]).reshape(2, 2).dot((r[:, sr] * np.repeat(v / rnorm, 2).reshape(-1, 2)).T).T

        seed = 66743015
        random.seed(seed)
        np.random.seed(seed)

        diff = 0.1
        r[:, svx] *= 1 + ((np.random.rand(self.n) * diff * 2) - diff)
        r[:, svy] *= 1 + ((np.random.rand(self.n) * diff * 2) - diff)

        self.r = r

    def __str__(self):
        return f'ImageGalaxy_{self.image_name}_{self.x}_{self.y}_{self.threshold}'
